import {
    LightningElement,
    api,
    track
} from 'lwc';

export default class path extends LightningElement {
    @track step;
    @api list;
    @api type = 'createorder';
    @track first = false;
    @api get currentStep() {
        return this.step;
    }

    connectedCallback() {
        setTimeout(function () {
            this.handleValueChange(this.step);
        }.bind(this), 1000);
    }

    set currentStep(value) {
        this.setAttribute('currentStep', value);
        this.step = value;
        if (this.first) {
            this.handleValueChange(value);
        }
        this.first = true;
    }

    handleValueChange(value) {
        let a = parseInt(value);
        for (let i = 0; i < a; i++) {
            let t = "test" + i;
            this.template.querySelector(`[data-id="${t}"]`).className = "slds-path__item slds-is-complete";
        }
        for (let i = a + 1; i <= 5; i++) {
            let t = "test" + i;
            this.template.querySelector(`[data-id="${t}"]`).className = "slds-path__item slds-is-incomplete";
        }
        let t = "test" + a;
        this.template.querySelector(`[data-id="${t}"]`).className = "slds-is-current slds-path__item";
    }
}